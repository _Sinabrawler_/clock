from math import *
import matplotlib.pyplot as plt
import datetime
import matplotlib.animation as animation
# ===========================================================
F = 0
cxy = 20
circle_r = 17
marker_r = 15
big_r = 12
small_r = 6

fig = plt.figure()
ax = fig.add_subplot(111)
# ===========================================================


def settime():
    now = datetime.datetime.now()
    s = now.second
    m = now.minute
    h = now.hour
    print('you started clock at =>', h, ':', m, ':', s)
    s_settime = int(input('write second : '))
    m_settime = int(input('write minute : '))
    h_settime = int(input('write hour : '))
    if(s == s_settime and m == m_settime and h == h_settime):
        F += 1


def marker():
    x = []
    y = []
    for i in range(0, 360, 30):
        x.append(cxy + marker_r*cos(radians(i)))
        y.append(cxy + marker_r*sin(radians(i)))
    return x, y
# ===========================================================


def calculate():
    now = datetime.datetime.now()
    s = now.second
    m = now.minute
    h = now.hour

    deg_s = (15 - s) * 3 * 2
    xs = [cxy, cxy + (big_r * cos(radians(deg_s)))]
    ys = [cxy, cxy + (big_r * sin(radians(deg_s)))]

    deg_m = (15 - m) * 6
    xm = [cxy, cxy + (big_r * cos(radians(deg_m)))]
    ym = [cxy, cxy + (big_r * sin(radians(deg_m)))]

    deg_h = ((3 - (h % 12)) * 30) - (m / 2)
    xh = [cxy, cxy + (small_r * cos(radians(deg_h)))]
    yh = [cxy, cxy + (small_r * sin(radians(deg_h)))]

    return [[xh, yh], [xm, ym], [xs, ys]]
# ===========================================================


def motion(i):
    res = calculate()

    ax.clear()
    ax.plot(res[2][0], res[2][1], color='r', linestyle='-.')
    ax.plot(res[1][0], res[1][1], color='b', linestyle='--')
    ax.plot(res[0][0], res[0][1], color='g')

    x, y = marker()
    ax.plot(x, y, color='y', linestyle=' ', marker='D')
    if(F == 0):
        ax.add_artist(plt.Circle((cxy, cxy), circle_r, fill=False))
        ax.set_aspect("equal")
        ax.axis([0, 40, 0, 40])
    if(F != 0):
        ax.add_artist(plt.Circle((cxy, cxy), circle_r, color='r', fill=True))
        ax.set_aspect("equal")
        ax.axis([0, 40, 0, 40])
# ===========================================================


anime = animation.FuncAnimation(fig, motion, interval=500)
settime()
plt.show()
